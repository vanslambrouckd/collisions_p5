var circles = [];

function setup() {
  createCanvas(640,300);

  // for (var i = 0; i < 10; i++) {
  //   var col = color(random(255), random(255), random(255));
  //   var circle = new Circle(random(0, width), random(0, height), random(5,20), col);
  //   circles.push(circle);
  // }
}


function draw() {
  background(0);

  for (var i = circles.length-1; i >= 0; i--) {
    circles[i].update();
    circles[i].show();
    for (var j = circles.length-1; j >= 0; j--) {
      if (i != j && circles[i].intersects(circles[j])) {
        circles[i].changeColor();
        circles[j].changeColor();
        print("intersects");
      }
    }
  }
}


function Circle(x, y, r, col) {
  this.loc = createVector(x, y);
  this.radius = r;
  this.col = col;

  this.update = function() {
    this.loc = createVector(random(this.loc.x-1, this.loc.x+1), random(this.loc.y-1, this.loc.y+1));
  }

  this.intersects = function(other) {
    var d = dist(this.loc.x, this.loc.y, other.loc.x, other.loc.y);
    if (d < this.radius+other.radius) {
        return true;
    }
    return false;
  }

  this.show = function() {
    noStroke();
    fill(this.col);
    ellipse(this.loc.x, this.loc.y, this.radius*2, this.radius*2);
  }

  this.changeColor = function() {
    this.col = color(random(255), random(255), 0);
  }
}

function mousePressed() {
  var circle = new Circle(mouseX, mouseY, 80, color(random(255),random(255),random(255)));
  circles.push(circle);
}
